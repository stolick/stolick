## Prerequisites

To indicate to the environment that Stolick is the desktop manager, it sets the `_NET_WM_WINDOW_TYPE_DESKTOP` atom according to the [EWMH specification](https://specifications.freedesktop.org/wm-spec/wm-spec-1.3.html). Therefore EWMH-compliant environments with a high degree of probability may not require any manual setup for Stolick to work properly. However, for this atom to install, Stolick must be run in X11 mode. If you are using X11 display server, then it will run in X11 mode automatically, but if you are running Wayland display server, this is usually done by setting the value of the `QT_QPA_PLATFORM` environment variable to `xcb`. In this case your Wayland display server must provide xwayland support.

## [labwc](https://github.com/labwc/labwc)

`rc.xml`

```
<labwc_config>
	<windowRules>
		<windowRule identifier="stolick">
			<skipTaskbar>yes</skipTaskbar>
			<skipWindowSwitcher>yes</skipWindowSwitcher>
			<fixedPosition>yes</fixedPosition>
			<action name="Maximize"/>
			<action name="ToggleDecorations"/>
			<action name="ToggleAlwaysOnBottom"/>
		</windowRule>
	</windowRules>
</labwc_config>
```

`autostart`

```
#!/bin/sh
stolick >/dev/null 2>/dev/null &
```
