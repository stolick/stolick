## Obtaining dependencies

| Operating system | Command                                                                                                     |
|------------------|-------------------------------------------------------------------------------------------------------------|
| Debian/Ubuntu    | `apt install build-essential cmake qt6-base-dev qt6-tools-dev libdb++-dev libglib2.0-bin ffmpegthumbnailer` |

## Compilation

```
cmake -B build
```
```
cmake --build build
```

## Installing (run as root)

```
cmake --install build
```
