## Why Stolick defaults to X11 backend (`QT_QPA_PLATFORM=xcb`)?

X11 provides functionality for windows to declare their type. EWMH-compliant
window managers/compositors can use information about window types to apply
special rules for how these windows should be presented. Stolick declares itself
as a desktop window and usually it means that it must not take focus, be always
in the background and be unlisted in taskbar.

There's similar technology in Wayland called Layer Shell, but it's not supported
by Qt upstream and there's only a third-party library which has a substantial bug
not fixed for a long time ([480240](https://bugs.kde.org/show_bug.cgi?id=480240)).
