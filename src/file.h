#ifndef STOLICK_FILE_H
#define STOLICK_FILE_H

#include <filesystem>
#include <unordered_set>
#include <QWidget>
#include <QLabel>
#include <QLayout>
#include <QMouseEvent>

using namespace std;

class Stolick_File: public QWidget
{
	Q_OBJECT
public:
	explicit Stolick_File(QWidget *parent = nullptr, filesystem::path path = "/dev/null");
	~Stolick_File();
	void mark_selected(void);
	void mark_unselected(void);
	bool overlaps_with(int x, int y, int w, int h);
	void update_icon(void);
	filesystem::path get_path(void);
	void full_move(int x, int y);
	void save_current_position_to_database(void);
	void mousePressEvent(QMouseEvent *e);
	void mouseMoveEvent(QMouseEvent *e);
	void mouseDoubleClickEvent(QMouseEvent *e);
	void contextMenuEvent(QContextMenuEvent *e);
	static bool compare_names_of_files(Stolick_File *a, Stolick_File *b);
private:
	void try_to_set_more_appropriate_icon_name(void);
	void run_blink_effect(void);
	void open(void);
	void launch(void);
	QLabel icon;
	QLabel label;
	QWidget label_shadow;
	QString thumbnail_path;
	filesystem::path path;
	bool is_directory;
	bool is_desktop_file; // .desktop file
	bool is_selected = false;
	string file_name;
	string icon_name;
	unordered_set<Stolick_File *> *selection;
	Q_SLOT void disable_animation();
signals:
};

#endif // STOLICK_FILE_H
