#ifndef STOLICK_CONFIG_H
#define STOLICK_CONFIG_H

#include <vector>
#include <string>
#include <filesystem>
#include <QColor>

#define dbg(...) fprintf(stderr, __VA_ARGS__);

using namespace std;

enum BgMode {STOLICK_STRETCH, STOLICK_FILL};

class Config
{
public:
	Config();
	bool init(void);

	void set_target_path(string path);
	string get_target_path(void);

	void set_bg_image(string path);
	string get_bg_image(void);

	void set_icon_size(int size);
	int get_icon_size(void);
	int get_icon_margin(void);
	int get_desktop_padding(void);

	vector<string> split_cmd_into_args(string cmd);
	void replace_all(vector<string> &haystacks, string needle, string value);
	void set_bg_mode(string bg_mode_str);
	void exec(vector<string> cmd_args);
	string get_thumbnail_path(filesystem::path path);
	void update_thumbnail(filesystem::path path);

	enum BgMode bg_mode = STOLICK_FILL;
	QColor bg_color = QColor(158, 115, 148);
	string icon_theme = "breeze";
	int font_size = 10;
	int grid_size = 0;
	string open_cmd = "xdg-open %f";
	string launch_cmd = "gio launch %f";
	string browse_cmd = "auto";
	string delete_cmd = "gio trash %f";
	string empty_cmd = "gio trash --empty";
	bool show_hidden_files = false;
	int confirm_del = 0;
private:
	string bg_image = "/dev/null";
	int icon_size = 64;
	int icon_margin = 42; // icon_size * 2 / 3
	int desktop_padding = 21; // icon_size / 3
	string target_path;
	string thumbnails_directory;
	void set_auto_config_settings(void);
	string get_config_file_path(void);
	bool parse(string path);
};

extern Config cfg;

#endif // STOLICK_CONFIG_H
