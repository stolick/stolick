#include <filesystem>
#include <QProcess>
#include <QCryptographicHash>
#include "config.h"

Config cfg;

Config::Config() {
	const char *cache = getenv("XDG_CACHE_HOME");
	if (cache != NULL) {
		this->thumbnails_directory = string(cache) + "/thumbnails/stolick/";
	} else {
		const char *home = getenv("HOME");
		this->thumbnails_directory = string(home) + "/.cache/thumbnails/stolick/";
	}
	filesystem::create_directories(this->thumbnails_directory);
}

bool
Config::init(void)
{
	string path = this->get_config_file_path();
	if (this->parse(path) == false) {
		return false;
	}
	this->set_auto_config_settings();
	return true;
}

void
Config::set_target_path(string path)
{
	// Path to the target directory must stay absolute, so that
	// file paths of different files used in database didn't converge.
	this->target_path = filesystem::absolute(path);
}

string
Config::get_target_path(void)
{
	return this->target_path;
}

void
Config::set_bg_image(string path)
{
	cfg.bg_image = filesystem::absolute(path);
}

string
Config::get_bg_image(void)
{
	return cfg.bg_image;
}

void
Config::set_icon_size(int size)
{
	cfg.icon_size = abs(size);
	cfg.icon_margin = cfg.icon_size * 2 / 3;
	cfg.desktop_padding = cfg.icon_size / 3;
}

int
Config::get_icon_size(void)
{
	return cfg.icon_size;
}

int
Config::get_icon_margin(void)
{
	return cfg.icon_margin;
}

int
Config::get_desktop_padding(void)
{
	return cfg.desktop_padding;
}

void
Config::replace_all(vector<string> &haystacks, string needle, string value)
{
	for (size_t i = 0; i < haystacks.size(); ++i) {
		string::size_type n = 0;
		while ((n = haystacks[i].find(needle, n)) != string::npos) {
			haystacks[i].replace(n, needle.size(), value);
			n += value.size();
		}
	}
}

vector<string>
Config::split_cmd_into_args(string cmd)
{
	string tmp;
	stringstream ss(cmd);
	vector<string> args;
	while(getline(ss, tmp, ' ')) {
		args.push_back(tmp);
	}
	return args;
}

void
Config::set_bg_mode(string bg_mode_str)
{
	if (bg_mode_str == "stretch") {
		this->bg_mode = STOLICK_STRETCH;
	}
}

inline bool
have_cmd(string cmd) {
	return system(("which " + cmd + " >/dev/null 2>/dev/null").c_str()) ? false : true;
}

void
Config::set_auto_config_settings(void)
{
	if (this->browse_cmd == "auto") {
		if      (have_cmd("pcmanfm"))    this->browse_cmd = "pcmanfm %f";
		else if (have_cmd("pcmanfm-qt")) this->browse_cmd = "pcmanfm-qt %f";
		else if (have_cmd("spacefm"))    this->browse_cmd = "spacefm %f";
		else if (have_cmd("thunar"))     this->browse_cmd = "thunar %f";
		else if (have_cmd("dolphin"))    this->browse_cmd = "dolphin %f";
		else if (have_cmd("nautilus"))   this->browse_cmd = "nautilus %f";
		else                             this->browse_cmd = "false";
	}
}

string
Config::get_config_file_path(void)
{
	string path;
	const char *xdg = getenv("XDG_CONFIG_HOME");
	if (xdg != nullptr) {
		path = string(xdg) + "/stolick/config";
		if (filesystem::exists(path)) {
			return path;
		}
	}
	const char *home = getenv("HOME");
	if (home != nullptr) {
		path = string(home) + "/.config/stolick/config";
		if (filesystem::exists(path)) {
			return path;
		}
		path = string(home) + "/.stolick/config";
		if (filesystem::exists(path)) {
			return path;
		}
	}
	return "/dev/null";
}

bool
Config::parse(string path)
{
	if (path == "/dev/null") {
		return true; // Config is optional, don't error out!
	}
	return true;
}

void
Config::exec(vector<string> cmd_args)
{
	QProcess process;
	process.setProgram(QString::fromStdString(cmd_args[0]));
	cmd_args.erase(cmd_args.begin());
	QVector<QString> a;
	transform(cmd_args.begin(), cmd_args.end(), back_inserter(a), [](const string &v){ return QString::fromStdString(v); });
	process.setArguments(a);
	process.startDetached();
}

string
Config::get_thumbnail_path(filesystem::path path)
{
	QCryptographicHash hash(QCryptographicHash::Md5);
	hash.addData(QByteArrayView(path.string().c_str(), path.string().size()));
	string hex = hash.result().toHex().toStdString();
	return this->thumbnails_directory + hex + ".jpg";
}

void
Config::update_thumbnail(filesystem::path path)
{
	if (filesystem::is_directory(path)) {
		return;
	}
	string thumbnail_path = cfg.get_thumbnail_path(path);
	string cmd("ffmpegthumbnailer -c jpeg -s %s -i %i -o %o");
	vector<string> args = cfg.split_cmd_into_args(cmd);
	cfg.replace_all(args, "%s", to_string(cfg.icon_size));
	cfg.replace_all(args, "%i", path);
	cfg.replace_all(args, "%o", thumbnail_path);
	cfg.exec(args);
}
