#ifndef STOLICK_DATABASE_H
#define STOLICK_DATABASE_H

#include <mutex>
#include <db_cxx.h>

using namespace std;

class Stolick_Database
{
	DbEnv *env;
	Db *db;
	mutex db_lock;
	string get_database_directory_path(void);
public:
	Stolick_Database();
	bool init(void);
	void close(void);
	void set_file_pos(string path, int x, int y);
	pair<int, int> get_file_pos(string path);
};

extern Stolick_Database db;

#endif // STOLICK_DATABASE_H
