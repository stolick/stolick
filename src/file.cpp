#include <cstdio>
#include <fstream>
#include <QString>
#include <QGraphicsBlurEffect>
#include <QGraphicsOpacityEffect>
#include <QPropertyAnimation>
#include <QMenu>
#include <QInputDialog>
#include <QMessageBox>
#include <QPainterPath>
#include <QDrag>
#include <QMimeData>
#include <QPainter>
#include "stolick.h"
#include "database.h"
#include "config.h"

static bool
ends_with(std::string const &fullString, std::string const &ending) {
	if (fullString.length() >= ending.length()) {
		return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
	} else {
		return false;
	}
}

Stolick_File::Stolick_File(QWidget *parent, filesystem::path path): QWidget{parent}
{
	this->path = path == "trash://" ? path : filesystem::absolute(path);
	this->file_name = path.string().substr(path.string().rfind("/") + 1);
	this->icon_name = this->file_name;
	this->is_directory = this->path == "trash://" || filesystem::is_directory(path);
	this->is_desktop_file = this->is_directory == false && ends_with(this->file_name, ".desktop");
	this->thumbnail_path = QString::fromStdString(cfg.get_thumbnail_path(this->path));
	this->selection = dynamic_cast<Stolick *>(parent)->get_files_selection();
	this->setParent(parent);
	this->setToolTip(QString::fromStdString(path.string()));
	this->setStyleSheet("background-color: #00000000;");
	this->setStyleSheet("QToolTip { color: #000000; background-color: #FFDF6B; }");
	this->resize(cfg.get_icon_size(), cfg.get_icon_size());
	this->try_to_set_more_appropriate_icon_name();

	this->icon.setParent(this);
	this->icon.setAlignment(Qt::AlignCenter);
	this->icon.resize(cfg.get_icon_size(), cfg.get_icon_size());
	this->update_icon();

	this->label.setParent(parent);
	this->label.setAttribute(Qt::WA_TransparentForMouseEvents);
	this->label.setStyleSheet("color: #FFFFFF; background-color: #00000000");
	this->label.setText(QString::fromStdString(this->icon_name));
	this->label.setFont(QFont("sans", cfg.font_size, QFont::Normal));
	this->label.adjustSize();
	// this->label.setWindowFlag(Qt::FramelessWindowHint);
	// this->label.setAlignment(Qt::AlignTop | Qt::AlignHCenter);
	// this->label.setWordWrap(true);

	this->label_shadow.setParent(parent);
	this->label_shadow.setAttribute(Qt::WA_TransparentForMouseEvents);
	this->label_shadow.resize(this->label.size().width() + cfg.font_size / 2, this->label.size().height());

	this->is_selected = true;
	this->mark_unselected();

	this->raise();
	this->show();
	this->label_shadow.raise();
	this->label_shadow.show();
	this->label.raise();
	this->label.show();
}

Stolick_File::~Stolick_File()
{
}

void
Stolick_File::mark_selected(void)
{
	if (this->is_selected == false) {
		this->is_selected = true;
		this->selection->insert(this);

		this->label_shadow.setStyleSheet("background-color: #2050F0;");
		this->label_shadow.setGraphicsEffect(nullptr);

		QPainterPath qpath;
		qpath.addRoundedRect(this->label_shadow.rect(), cfg.font_size / 4, cfg.font_size / 4);
		QRegion mask = QRegion(qpath.toFillPolygon().toPolygon());
		this->label_shadow.setMask(mask);
	}
}

void
Stolick_File::mark_unselected(void)
{
	if (this->is_selected == true) {
		this->is_selected = false;
		this->selection->erase(this);

		this->label_shadow.clearMask();

		this->label_shadow.setStyleSheet("background-color: rgba(0, 0, 0, 80);");
		auto effect = new QGraphicsBlurEffect();
		effect->setBlurRadius(cfg.font_size);
		this->label_shadow.setGraphicsEffect(effect); // takes ownership of effect
	}
}

bool
Stolick_File::overlaps_with(int x, int y, int w, int h)
{
	if ((this->pos().x() < x) && (this->pos().x() + this->width() >= x)
		|| (x < this->pos().x()) && (x + w >= this->pos().x()))
	{
		if ((this->pos().y() < y) && (this->pos().y() + this->height() >= y)
			|| (y < this->pos().y()) && (y + h >= this->pos().y()))
		{
			return true;
		}
	}
	return false;
}

static string
get_value_from_desktop_file(string path, string key)
{
	ifstream is(path);
	string str;
	while (getline(is, str)) {
		if (str.rfind(key, 0) == 0) {
			str.erase(0, key.length());
			dbg("Found '%s' key in .desktop file: %s\n", key.c_str(), str.c_str());
			return str;
		}
	}
	dbg("Not found '%s' key in .desktop file\n", key.c_str());
	return string();
}

void
Stolick_File::try_to_set_more_appropriate_icon_name(void)
{
	if (this->is_desktop_file) {
		string name = get_value_from_desktop_file(this->path, "Name=");
		if (name.length() > 0) {
			this->icon_name = name;
		}
	} else if (this->path == "trash://") {
		this->icon_name = "Trash";
	}
}

void
Stolick_File::disable_animation()
{
	this->icon.setGraphicsEffect(nullptr);
}

void
Stolick_File::run_blink_effect(void)
{
	QGraphicsOpacityEffect *effect = new QGraphicsOpacityEffect(&this->icon);
	this->icon.setGraphicsEffect(effect);
	QPropertyAnimation *a = new QPropertyAnimation(effect, "opacity");
	a->setDuration(400);
	a->setStartValue(1);
	a->setKeyValueAt(0.8, 0.5);
	a->setEndValue(1);
	a->setEasingCurve(QEasingCurve::OutExpo);
	connect(a, SIGNAL(finished()), this, SLOT(disable_animation()));
	a->start(QAbstractAnimation::DeleteWhenStopped);
}

void
Stolick_File::open(void)
{
	vector<string> args = cfg.split_cmd_into_args(this->is_directory ? cfg.browse_cmd : cfg.open_cmd);
	cfg.replace_all(args, "%f", this->path);
	cfg.exec(args);
	this->run_blink_effect();
}

void
Stolick_File::launch(void)
{
	vector<string> args = cfg.split_cmd_into_args(cfg.launch_cmd);
	cfg.replace_all(args, "%f", this->path);
	cfg.exec(args);
	this->run_blink_effect();
}

void
Stolick_File::update_icon(void)
{
	if (this->is_desktop_file) {
		string name = get_value_from_desktop_file(this->path, "Icon=");
		if (name.size() > 0) {
			QIcon desktop_icon = QIcon::fromTheme(QString::fromStdString(name));
			if (!desktop_icon.isNull()) {
				this->icon.setPixmap(desktop_icon.pixmap(cfg.get_icon_size(), cfg.get_icon_size()));
				return;
			}
		}
	} else if (this->path == "trash://") {
		QIcon trash_icon = QIcon::fromTheme("user-trash");
		if (!trash_icon.isNull()) {
			this->icon.setPixmap(trash_icon.pixmap(cfg.get_icon_size(), cfg.get_icon_size()));
		} else {
			this->icon.setStyleSheet("background-color: #000000;");
		}
		return;
	}
	QPixmap thumbnail_pixmap;
	if (this->is_directory == false && thumbnail_pixmap.load(this->thumbnail_path) == true) {
		this->icon.setPixmap(thumbnail_pixmap.scaled(cfg.get_icon_size(), cfg.get_icon_size(), Qt::KeepAspectRatio));
		return;
	}
	QIcon icon = dynamic_cast<Stolick *>(this->parentWidget())->get_icon_for_file(this->path);
	if (!icon.isNull()) {
		this->icon.setPixmap(icon.pixmap(cfg.get_icon_size(), cfg.get_icon_size()));
	} else {
		this->icon.setStyleSheet("background-color: #000000;");
	}
}

filesystem::path
Stolick_File::get_path(void)
{
	return this->path;
}

void
Stolick_File::full_move(int x, int y)
{
	auto this_size = this->size().width();
	QSize parent_size = this->parentWidget()->size();
	int x_limit = parent_size.width(), y_limit = parent_size.height();
	if (x < 0) x = 0;
	if (y < 0) y = 0;
	if (x + this_size > x_limit) x = x_limit - this_size;
	if (y + this_size > y_limit) y = y_limit - this_size;
	if (cfg.grid_size > 1) {
		QPoint icon_center = this->rect().center();
		x += icon_center.x();
		y += icon_center.x();
		x = x - x % cfg.grid_size;
		y = y - y % cfg.grid_size;
	}
	this->move(x, y);
	this->label_shadow.move(
		x + (this->size().width() - this->label_shadow.size().width()) / 2,
		y + this->size().height()
	);
	this->label.move(
		x + (this->size().width() - this->label.size().width()) / 2,
		y + this->size().height()
	);
}

void
Stolick_File::save_current_position_to_database(void)
{
	db.set_file_pos(this->path.string(), this->x(), this->y());
}

void
Stolick_File::mousePressEvent(QMouseEvent *e)
{
	if (this->is_selected == false) {
		dynamic_cast<Stolick *>(this->parentWidget())->clear_files_from_selection();
	}
	this->mark_selected();
}

// We have to call drag action on mouse move event and not on mouse press event,
// because mouse press event is called together with context menu event.
// That is, we do it to avoid calling QDrag and QMenu simultaneously.
void
Stolick_File::mouseMoveEvent(QMouseEvent *e)
{
	Stolick *parent = dynamic_cast<Stolick *>(this->parentWidget());
	parent->drag_start_pos = this->pos() + e->pos();

	QPixmap ghost(parent->width(), parent->height());
	ghost.fill(QColor(0, 0, 0, 0));
	QPainter ghost_painter(&ghost);
	ghost_painter.setOpacity(0.5);
	for (auto i = this->selection->begin(); i != this->selection->end(); ++i) {
		ghost_painter.drawPixmap((*i)->x(), (*i)->y(), (*i)->icon.pixmap());
	}

	QDrag *drag = new QDrag(this);
	drag->setPixmap(ghost);
	drag->setHotSpot(QPoint(parent->drag_start_pos.x(), parent->drag_start_pos.y()));

	QList<QUrl> files;
	for (auto i = this->selection->begin(); i != this->selection->end(); ++i) {
		files.append(QUrl::fromLocalFile(QString::fromStdString((*i)->path.string())));
	}

	QMimeData *mimeData = new QMimeData;
	mimeData->setUrls(files);
	drag->setMimeData(mimeData);

	Qt::DropAction dropAction = drag->exec(Qt::CopyAction | Qt::MoveAction);
}

void
Stolick_File::mouseDoubleClickEvent(QMouseEvent *e)
{
	(void)e;
	if (this->is_desktop_file) {
		this->launch();
	} else {
		this->open();
	}
}

void
Stolick_File::contextMenuEvent(QContextMenuEvent *e)
{
	QMenu menu(this);
	if (this->selection->size() == 1) {
		if (this->is_desktop_file) {
			menu.addAction("Launch");
		} else {
			menu.addAction("Open");
		}
	}
	if (this->path != "trash://") {
		menu.addAction("Copy", QKeySequence::Copy);
		menu.addAction("Rename");
		menu.addAction("Delete", QKeySequence::Delete);
	} else {
		menu.addAction("Clear");
	}
	QAction *action = menu.exec(e->globalPos());
	if (action == nullptr) return;

	string action_str = action->text().toStdString();

	if (action_str == "Open") {
		this->open();
	} else if (action_str == "Launch") {
		this->launch();
	} else if (action_str == "Copy") {
		dynamic_cast<Stolick *>(this->parentWidget())->copy_selected_files();
	} else if (action_str == "Rename") {
		string msg = "Rename " + this->file_name + " to:";
		QString text = QInputDialog::getText(this, "Rename file", QString::fromStdString(msg));
		// if (text.size() > 0) {
		// 	int pos_x = this->x();
		// 	int pos_y = this->y();
		// 	if (!rename(this->file_name.c_str(), text.toStdString().c_str())) {

		// 	}
		// }
		// fprintf(stderr, "%s\n", text.toStdString().c_str());
	} else if (action_str == "Delete") {
		dynamic_cast<Stolick *>(this->parentWidget())->delete_selected_files();
	} else if (action_str == "Clear") {
		QMessageBox prompt;
		prompt.setText("Are you sure you want to clear the trash?");
		prompt.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
		prompt.setDefaultButton(QMessageBox::Cancel);
		if (prompt.exec() == QMessageBox::Yes) {
			vector<string> args = cfg.split_cmd_into_args(cfg.empty_cmd);
			cfg.exec(args);
		}
	}
}

bool
Stolick_File::compare_names_of_files(Stolick_File *a, Stolick_File *b)
{
	return a->file_name.compare(b->file_name) <= 0;
}
