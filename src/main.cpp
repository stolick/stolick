#include <cstdlib>
#include <QApplication>
#include <QLocale>
#include <QTranslator>
#include <QScreen>
#include <QCommandLineParser>
#include "stolick.h"
#include "database.h"
#include "config.h"

struct stolick_arg {
	string short_opt;
	string long_opt;
	string description;
	string val;
	QCommandLineOption *opt;
};

bool
change_to_target_directory(void)
{
	string dir = cfg.get_target_path();
	try {
		filesystem::create_directory(dir);
		filesystem::current_path(dir);
	} catch (...) {
		cerr << "Failed to initialize target directory!\n";
		return false;
	}
	return true;
}

int
main(int argc, char *argv[])
{
	// See doc/FAQ.md on why we default to X11 backend
	bool run_wayland = false;
	for (size_t i = 1; i < argc; ++i) {
		if (strcmp(argv[i], "-W") == 0 || strcmp(argv[i], "--run-wayland") == 0) {
			run_wayland = true;
		}
	}
	if (run_wayland == false) {
		setenv("QT_QPA_PLATFORM", "xcb", 1);
	}

	QApplication app(argc, argv);
	QCoreApplication::setApplicationName("Stolick");
	QCoreApplication::setApplicationVersion("0.0.1");

	map<string, string> m;
	struct stolick_arg args[] = {
		{"b", "bg-image",    "Background image file (default: /dev/null)",                         "path",         NULL},
		{"m", "bg-mode",     "Background image display mode (default: fill)",                      "stretch|fill", NULL},
		{"c", "bg-color",    "Background color (default: #9E7394)",                                "color",        NULL},
		{"i", "icon-size",   "Size of icons in pixels (default: 64)",                              "size",         NULL},
		{"t", "icon-theme",  "Icon theme (default: breeze)",                                       "theme",        NULL},
		{"f", "icon-font",   "Font of icon labels (default: \"sans 10\")",                         "font",         NULL},
		{"g", "grid-size",   "Size of grid in pixels (default: 0)",                                "size",         NULL},
		{"O", "open-cmd",    "Command to open files (default: \"xdg-open %f\")",                   "command",      NULL},
		{"L", "launch-cmd",  "Command to launch desktop entries (default: \"gio launch %f\")",     "command",      NULL},
		{"B", "browse-cmd",  "Command to browse directories (default: auto)",                      "command",      NULL},
		{"D", "delete-cmd",  "Command to delete files (default: \"gio trash %f\")",                "command",      NULL},
		{"E", "empty-cmd",   "Command to empty trash (default: \"gio trash --empty\")",            "command",      NULL},
		{"C", "confirm-del", "Files count threshold requiring deletion confirmation (default: 0)", "threshold",    NULL},
		{"~", "~",           "~",                                                                  "~",            NULL},
	};
	QCommandLineOption show_hidden_opt(QStringList() << "H" << "show-hidden", "Show hidden files (default: no)");
	QCommandLineOption run_wayland_opt(QStringList() << "W" << "run-wayland", "Run Wayland backend (default: no)");

	QCommandLineParser parser;
	parser.setApplicationDescription("Standalone desktop file manager");
	parser.addPositionalArgument("path", "Directory to open (default: \"$HOME/Desktop\")");
	for (size_t i = 0; args[i].short_opt != "~"; ++i) {
		args[i].opt = new QCommandLineOption(
			QStringList() << QString::fromStdString(args[i].short_opt) << QString::fromStdString(args[i].long_opt),
			QString::fromStdString(args[i].description),
			QString::fromStdString(args[i].val)
		);
		parser.addOption(*args[i].opt);
	}
	parser.addOption(show_hidden_opt);
	parser.addOption(run_wayland_opt); // This option was processed above already!
	parser.addVersionOption();
	parser.addHelpOption();
	parser.process(app);

	for (size_t i = 0; args[i].short_opt != "~"; ++i) {
		args[i].val = parser.value(*args[i].opt).toStdString();
		m.insert({args[i].short_opt, args[i].val});
	}
	if (m.at("b").size()) cfg.set_bg_image(m.at("b"));
	if (m.at("m").size()) cfg.set_bg_mode(m.at("m"));
	if (m.at("c").size()) cfg.bg_color   = QColor(QString::fromStdString(m.at("c")));
	if (m.at("i").size()) cfg.set_icon_size(stoi(m.at("i")));
	if (m.at("t").size()) cfg.icon_theme = m.at("t");
	if (m.at("f").size()) {}
	if (m.at("g").size()) cfg.grid_size   = abs(stoi(m.at("g")));
	if (m.at("O").size()) cfg.open_cmd    = m.at("O");
	if (m.at("L").size()) cfg.launch_cmd  = m.at("L");
	if (m.at("B").size()) cfg.browse_cmd  = m.at("B");
	if (m.at("D").size()) cfg.delete_cmd  = m.at("D");
	if (m.at("E").size()) cfg.empty_cmd   = m.at("E");
	if (m.at("C").size()) cfg.confirm_del = stoi(m.at("C"));
	cfg.show_hidden_files                 = parser.isSet(show_hidden_opt);

	const QStringList qargs = parser.positionalArguments();
	if (qargs.size() > 0) {
		cfg.set_target_path(qargs.at(0).toStdString());
	} else {
		cfg.set_target_path(string(getenv("HOME")) + "/Desktop");
	}

	// -- TODO --
	//QTranslator translator;
	//const QStringList uiLanguages = QLocale::system().uiLanguages();
	//for (const QString &locale : uiLanguages) {
	//	const QString baseName = "stolick_" + QLocale(locale).name();
	//	if (translator.load(":/i18n/" + baseName)) {
	//		a.installTranslator(&translator);
	//		break;
	//	}
	//}
	// -- TODO --

	QIcon::setThemeName(QString::fromStdString(cfg.icon_theme));

	if (change_to_target_directory() == false) return 1;
	if (cfg.init()                   == false) return 1;
	if (db.init()                    == false) return 1;

	QScreen *screen = QGuiApplication::primaryScreen();
	QRect rect = screen->availableGeometry();

	Stolick w(nullptr, rect.width(), rect.height());
	auto status = app.exec();
	db.close();
	return status;
}
