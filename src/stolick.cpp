#include <cstdio>
#include <QApplication>
#include <QClipboard>
#include <QLayout>
#include <QTimer>
#include <QMenu>
#include <QAction>
#include <QPainter>
#include <QGraphicsOpacityEffect>
#include <QMessageBox>
#include <QInputDialog>
#include <QMimeData>
#include <QShortcut>
#include "stolick.h"
#include "database.h"
#include "config.h"

Stolick::Stolick(QWidget *parent, int width, int height): QMainWindow(parent)
{
	this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint | Qt::WindowStaysOnBottomHint);
	// this->setFocusPolicy(Qt::NoFocus); // Needs research...
	this->setAttribute(Qt::WA_X11NetWmWindowTypeDesktop); // sets _NET_WM_WINDOW_TYPE_DESKTOP atom
	this->setAttribute(Qt::WA_DeleteOnClose);
	this->resize(width, height);
	this->pixmap.load(QString::fromStdString(cfg.get_bg_image()));

	this->select_box.hide();
	this->select_box.setParent(this);
	this->select_box.setStyleSheet("background-color: #302050F0; border: 0.1em solid #2050F0;");

	this->show();

	pair<int, int> trash_xy = db.get_file_pos("trash://");
	this->trash_icon = new Stolick_File(this, "trash://");
	this->trash_icon->full_move(get<0>(trash_xy), get<1>(trash_xy));

	QShortcut *copy_bind = new QShortcut(QKeySequence::Copy, this);
	QObject::connect(copy_bind, SIGNAL(activated()), this, SLOT(copy_selected_files()));
	QShortcut *delete_bind = new QShortcut(QKeySequence::Delete, this);
	QObject::connect(delete_bind, SIGNAL(activated()), this, SLOT(delete_selected_files()));

	this->update_files();
	this->update_timer = new QTimer(this);
	connect(this->update_timer, &QTimer::timeout, this, &Stolick::update_files);
	this->update_timer->start(2000);
}

Stolick::~Stolick()
{
	delete this->update_timer;
}

QIcon
Stolick::get_icon_for_file(string path)
{
	QFileInfo file_info(QString::fromStdString(path));
	// Good icon for files is "text-plain"
	// Good icon for directories is "inode-directory"
	return this->icon_provider.icon(file_info);
}

void
Stolick::contextMenuEvent(QContextMenuEvent *e)
{
	QMenu menu(this);

	QAction *paste = new QAction("Paste");
	paste->setEnabled(false);
	menu.addAction("New directory");
	menu.addAction(paste);
	menu.addAction("Sort by name");

	QAction *action = menu.exec(e->globalPos());
	if (action == nullptr) return;

	string action_str = action->text().toStdString();

	if (action_str == "New directory") {
		QString name = QInputDialog::getText(this, "Create new directory", "New directory name: ");
		if (name.length() > 0) {
			filesystem::path path(name.toStdString());
			path = filesystem::absolute(path);
			int x = e->pos().x();
			int y = e->pos().y();
			if (x >= cfg.get_icon_size() / 2) x -= cfg.get_icon_size() / 2;
			if (y >= cfg.get_icon_size() / 2) y -= cfg.get_icon_size() / 2;
			db.set_file_pos(path.string(), x, y);
			filesystem::create_directory(path);
			this->burst_desktop_updates();
		}
	} else if (action_str == "Sort by name") {
		this->update_lock.lock();
		sort(this->icons.begin(), this->icons.end(), Stolick_File::compare_names_of_files);
		this->rearrange_icons();
		this->update_lock.unlock();
	}
}

void
Stolick::paintEvent(QPaintEvent *e)
{
	(void)e;
	QPainter paint(this);
	if (this->pixmap.isNull()) {
		paint.fillRect(this->rect(), QBrush(cfg.bg_color, Qt::SolidPattern));
	} else if (cfg.bg_mode == STOLICK_STRETCH) {
		QPixmap new_pixmap = this->pixmap.scaled(this->width(), this->height(), Qt::IgnoreAspectRatio);
		paint.drawPixmap(0, 0, new_pixmap);
	} else { // STOLICK_FILL
		QPixmap new_pixmap = this->pixmap.scaled(this->width(), this->height(), Qt::KeepAspectRatioByExpanding);
		QRect pixmap_box = new_pixmap.rect();
		pixmap_box.moveCenter(this->rect().center());
		paint.drawPixmap(pixmap_box.topLeft(), new_pixmap);
	}
}

void
Stolick::update_files(void)
{
	this->update_lock.lock();

	int desktop_origin = cfg.get_desktop_padding();
	int icon_step = cfg.get_icon_size() + cfg.get_icon_margin();
	int icon_x = desktop_origin;
	int icon_y = desktop_origin;

	vector<filesystem::path> files;
	for (const auto &entry: filesystem::directory_iterator(cfg.get_target_path())) {
		files.push_back(filesystem::absolute(entry.path()));
	}

	// Remove deleted files
	for (size_t i = 0; i < this->icons.size(); ++i) {
		size_t j = 0;
		while (j < files.size() && files[j] != this->icons[i]->get_path()) {
			j += 1;
		}
		if (j >= files.size()) {
			this->icons[i]->deleteLater();
			this->icons.erase(this->icons.begin() + i);
		}
	}

	// Add new files to desktop
	for (size_t i = 0; i < files.size(); ++i) {
		size_t j = 0;
		while (j < this->icons.size() && this->icons[j]->get_path() != files[i]) {
			j += 1;
		}
		string filename = files[i].string().substr(files[i].string().rfind("/") + 1);
		if (cfg.show_hidden_files == false && filename.c_str()[0] == '.') {
			continue; // Ignore hidden file
		}
		if (j < this->icons.size()) {
			// File still exists
		} else {
			cfg.update_thumbnail(files[i]);
			Stolick_File *icon = new Stolick_File(this, files[i]);
			pair<int, int> xy = db.get_file_pos(files[i]);
			int x = get<0>(xy), y = get<1>(xy);
			if (x == 0 && y == 0) {
				x = icon_x;
				y = icon_y;
				if (icon_y + icon_step + cfg.get_desktop_padding() + cfg.font_size * 6 < this->size().height()) {
					icon_y += icon_step;
				} else if (icon_x + icon_step + cfg.get_desktop_padding() < this->size().width()) {
					icon_x += icon_step;
					icon_y = desktop_origin;
				} else {
					desktop_origin += cfg.get_icon_size() / 3;
					icon_x = desktop_origin;
					icon_y = desktop_origin;
				}
			}
			icon->full_move(x, y);
			icon->save_current_position_to_database();
			this->icons.push_back(icon);
		}
	}

	this->update_lock.unlock();
}

void
Stolick::rearrange_icons(void)
{
	int desktop_origin = cfg.get_desktop_padding();
	int icon_step = cfg.get_icon_size() + cfg.get_icon_margin();
	int icon_x = desktop_origin;
	int icon_y = desktop_origin;
	for (size_t i = 0; i < this->icons.size(); ++i) {
		this->icons[i]->full_move(icon_x, icon_y);
		this->icons[i]->save_current_position_to_database();
		if (icon_y + icon_step + cfg.get_desktop_padding() + cfg.font_size * 6 < this->size().height()) {
			icon_y += icon_step;
		} else if (icon_x + icon_step + cfg.get_desktop_padding() < this->size().width()) {
			icon_x += icon_step;
			icon_y = desktop_origin;
		} else {
			desktop_origin += cfg.get_icon_size() / 3;
			icon_x = desktop_origin;
			icon_y = desktop_origin;
		}
	}
}

void
Stolick::burst_desktop_updates(void)
{
	for (int i = 50; i < 1000; i = i * 3 / 2) {
		QTimer::singleShot(i, this, &Stolick::update_files);
	}
}

void
Stolick::clear_files_from_selection(void)
{
	this->selected_icons.clear();
	for (size_t i = 0; i < this->icons.size(); ++i) {
		this->icons[i]->mark_unselected();
	}
	this->trash_icon->mark_unselected();
}

unordered_set<Stolick_File *> *
Stolick::get_files_selection(void)
{
	return &this->selected_icons;
}

void
Stolick::copy_selected_files(void)
{
	QMimeData *mimeData = new QMimeData;
	QList<QUrl> files;
	for (auto i = this->selected_icons.begin(); i != this->selected_icons.end(); ++i) {
		files.append(QUrl::fromLocalFile(QString::fromStdString((*i)->get_path().string())));
	}
	mimeData->setUrls(files);
	QApplication::clipboard()->setMimeData(mimeData); // Takes ownership
}

void
Stolick::delete_selected_files(void)
{
	size_t selected_icons_count = this->selected_icons.size();
	if (cfg.confirm_del > 0 && selected_icons_count >= cfg.confirm_del) {
		string msg = "Are you sure you want to delete " + to_string(selected_icons_count) + " files?";
		QMessageBox prompt;
		prompt.setText(QString::fromStdString(msg));
		prompt.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
		prompt.setDefaultButton(QMessageBox::Cancel);
		if (prompt.exec() != QMessageBox::Yes) {
			return;
		}
	}
	for (auto i = this->selected_icons.begin(); i != this->selected_icons.end(); ++i) {
		vector<string> args = cfg.split_cmd_into_args(cfg.delete_cmd);
		cfg.replace_all(args, "%f", (*i)->get_path().string());
		cfg.exec(args);
	}
	this->burst_desktop_updates();
}

void
Stolick::mousePressEvent(QMouseEvent *e)
{
	this->select_box_start = e->pos();
	this->clear_files_from_selection();
}

void
Stolick::mouseMoveEvent(QMouseEvent *e)
{
	QPoint pos = e->pos();
	int x = min(this->select_box_start.x(),  pos.x());
	int y = min(this->select_box_start.y(),  pos.y());
	int w = abs(this->select_box_start.x() - pos.x());
	int h = abs(this->select_box_start.y() - pos.y());
	this->select_box.setGeometry(x, y, w, h);
	this->select_box.raise();
	this->select_box.show();

	for (size_t i = 0; i < this->icons.size(); ++i) {
		if (this->icons[i]->overlaps_with(x, y, w, h)) {
			this->icons[i]->mark_selected();
		} else {
			this->icons[i]->mark_unselected();
		}
	}
	if (this->trash_icon->overlaps_with(x, y, w, h)) {
		this->trash_icon->mark_selected();
	} else {
		this->trash_icon->mark_unselected();
	}
}

void
Stolick::mouseReleaseEvent(QMouseEvent *e)
{
	this->select_box.hide();
}

void
Stolick::dragEnterEvent(QDragEnterEvent *e)
{
	e->acceptProposedAction();
}

void
Stolick::dropEvent(QDropEvent *e)
{
	if (e->source()) { // Source of the drag operation is this application
		QPoint delta = e->position().toPoint() - this->drag_start_pos;
		for (auto i = this->selected_icons.begin(); i != this->selected_icons.end(); ++i) {
			(*i)->full_move((*i)->x() + delta.x(), (*i)->y() + delta.y());
			(*i)->save_current_position_to_database();
		}
	} else if (e->mimeData()->hasUrls() && (e->proposedAction() & (Qt::CopyAction | Qt::MoveAction))) {
		e->acceptProposedAction();

		int x = e->position().toPoint().x() - cfg.get_icon_size() / 2;
		int y = e->position().toPoint().y() - cfg.get_icon_size() / 2;
		foreach(QUrl url, e->mimeData()->urls()) {
			QString old_path = url.path(QUrl::FullyDecoded);
			QFileInfo info(old_path);
			QFile file(old_path);
			string new_path = cfg.get_target_path() + "/" + info.fileName().toStdString();

			db.set_file_pos(new_path, x, y);

			switch (e->proposedAction()) {
				case Qt::CopyAction: file.copy(QString::fromStdString(new_path));   break;
				case Qt::MoveAction: file.rename(QString::fromStdString(new_path)); break;
			}

			y += cfg.get_icon_size() + cfg.get_icon_margin();
		}

		this->burst_desktop_updates();
	}
}
