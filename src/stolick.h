#ifndef STOLICK_H
#define STOLICK_H

#include <unordered_set>
#include <mutex>
#include <QMainWindow>
#include <QFileIconProvider>
#include <QDragEnterEvent>
#include <QDropEvent>
#include "file.h"

using namespace std;

class Stolick: public QMainWindow
{
	Q_OBJECT
	vector<Stolick_File *> icons;
	unordered_set<Stolick_File *> selected_icons;
	Stolick_File *trash_icon;
	QTimer *update_timer;
	QFileIconProvider icon_provider;
	QWidget select_box;
	QPoint select_box_start;
	QPixmap pixmap;
	mutex update_lock;
	void update_files(void);
	void rearrange_icons(void);
public:
	Stolick(QWidget *parent = nullptr, int width = 0, int height = 0);
	~Stolick();
	QPoint drag_start_pos;
	void burst_desktop_updates(void);
	QIcon get_icon_for_file(string path);
	void contextMenuEvent(QContextMenuEvent *e);
	void paintEvent(QPaintEvent *e);
	void clear_files_from_selection(void);
	unordered_set<Stolick_File *> *get_files_selection(void);
	Q_SLOT void copy_selected_files(void);
	Q_SLOT void delete_selected_files(void);
	void mousePressEvent(QMouseEvent *e);
	void mouseMoveEvent(QMouseEvent *e);
	void mouseReleaseEvent(QMouseEvent *e);
	void dragEnterEvent(QDragEnterEvent *e);
	void dropEvent(QDropEvent *e);
};
#endif // STOLICK_H
