#include <cstdlib>
#include <filesystem>
#include "database.h"

#define STOLICK_DATABASE_FILENAME "stolick.bdb"

Stolick_Database db;

Stolick_Database::Stolick_Database() {}

bool
Stolick_Database::init(void)
{
	string path = this->get_database_directory_path();
	if (path == "/dev/null") {
		cerr << "Failed to obtain database directory!\n";
		return false;
	}
	this->env = new DbEnv(0);
	if (this->env->open(path.c_str(), DB_CREATE | DB_INIT_MPOOL, 0) != 0) {
		cerr << "Failed to initialize database environment!\n";
		return false;
	}
	this->db = new Db(this->env, 0);
	if (this->db->open(NULL, (path + STOLICK_DATABASE_FILENAME).c_str(), NULL, DB_BTREE, DB_CREATE, 0) != 0) {
		cerr << "Failed to initialize database!\n";
		return false;
	}
	return true;
}

void
Stolick_Database::close(void)
{
	this->db->close(0);
	this->env->close(0);
	delete db;
	delete env;
}

string
Stolick_Database::get_database_directory_path(void)
{
	string path;
	const char *xdg = getenv("XDG_DATA_HOME");
	if (xdg != nullptr) {
		path = string(xdg) + "/stolick/";
		if (filesystem::exists(path + STOLICK_DATABASE_FILENAME)) {
			return path;
		}
	}
	const char *home = getenv("HOME");
	if (home != nullptr) {
		path = string(home) + "/.local/share/stolick/";
		if (filesystem::exists(path + STOLICK_DATABASE_FILENAME)) {
			return path;
		}
		path = string(home) + "/.stolick/";
		if (filesystem::exists(path + STOLICK_DATABASE_FILENAME)) {
			return path;
		}
	}
	if (xdg != nullptr) {
		path = string(xdg) + "/stolick/";
		filesystem::create_directories(path);
		if (filesystem::exists(path)) {
			return path;
		}
	}
	if (home != nullptr) {
		path = string(home) + "/.local/share/stolick/";
		filesystem::create_directories(path);
		if (filesystem::exists(path)) {
			return path;
		}
	}
	return "/dev/null";
}

void
Stolick_Database::set_file_pos(string path, int x, int y)
{
	if (x < 0) x = 0;
	if (y < 0) y = 0;
	this->db_lock.lock();
	char tmp[512];
	sprintf(tmp, "%d,%d", x, y);
	string xy(tmp);
	Dbt key(const_cast<char *>(path.data()), path.size() + 1);
	Dbt value(const_cast<char *>(xy.data()), xy.size() + 1);
	// Dbt key(const_cast<char *>("haha1"), 6);
	// Dbt value(const_cast<char *>("haha2"), 6);
	this->db->put(NULL, &key, &value, 0);
	this->db_lock.unlock();
}

pair<int, int>
Stolick_Database::get_file_pos(string path)
{
	int x = 0, y = 0;
	this->db_lock.lock();
	Dbt key(const_cast<char *>(path.data()), path.size() + 1);
	Dbt value;
	this->db->get(NULL, &key, &value, 0);
	const char *raw_data = (const char *)value.get_data();
	size_t raw_size = value.get_size();
	string data(raw_data, raw_size);
	sscanf(data.c_str(), "%d,%d", &x, &y);
	this->db_lock.unlock();
	return pair(x, y);
}
