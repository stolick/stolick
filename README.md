## Description

Stolick is a standalone desktop file manager.

![Stolick in action](doc/stolick.jpg)

## Features

* Free & gridded icons placement
* Highly customizable configuration
* Icon thumbnails functionality
* Trash support

## Dependencies

| Name                                                              | Version   | Necessity                |
|-------------------------------------------------------------------|-----------|--------------------------|
| [Qt](https://doc.qt.io)                                           | >= 6.6.0  | required                 |
| [Berkeley DB](https://en.wikipedia.org/wiki/Berkeley_DB)          | >= 5.3.28 | required                 |
| C++ compiler                                                      | Any C++17 | required at build-time   |
| [CMake](https://cmake.org)                                        | >= 3.27.6 | required at build-time   |
| [glib](https://docs.gtk.org/glib)                                 | >= 2.79.1 | optional, for trash      |
| [ffmpegthumbnailer](https://github.com/dirkvdb/ffmpegthumbnailer) | >= 2.2.2  | optional, for thumbnails |

## Installing

Detailed build instructions for different platforms are described in
[doc/build-instructions.md](https://codeberg.org/stolick/stolick/src/branch/master/doc/build-instructions.md).

## Learning more

Basic information about Stolick can be obtained with `stolick -h` command:

```
Usage: stolick [options] path
Standalone desktop file manager

Options:
  -b, --bg-image <path>          Background image file (default: /dev/null)
  -m, --bg-mode <stretch|fill>   Background image display mode (default: fill)
  -c, --bg-color <color>         Background color (default: #9E7394)
  -i, --icon-size <size>         Size of icons in pixels (default: 64)
  -t, --icon-theme <theme>       Icon theme (default: breeze)
  -f, --icon-font <font>         Font of icon labels (default: "sans 10")
  -g, --grid-size <size>         Size of grid in pixels (default: 0)
  -O, --open-cmd <command>       Command to open files (default: "xdg-open %f")
  -L, --launch-cmd <command>     Command to launch desktop entries (default: "gio launch %f")
  -B, --browse-cmd <command>     Command to browse directories (default: auto)
  -D, --delete-cmd <command>     Command to delete files (default: "gio trash %f")
  -E, --empty-cmd <command>      Command to empty trash (default: "gio trash --empty")
  -C, --confirm-del <threshold>  Files count threshold requiring deletion confirmation (default: 0)
  -H, --show-hidden              Show hidden files (default: no)
  -W, --run-wayland              Run Wayland backend (default: no)
  -v, --version                  Displays version information.
  -h, --help                     Displays help on commandline options.
  --help-all                     Displays help, including generic Qt options.

Arguments:
  path                           Directory to open (default: "$HOME/Desktop")
```

Also there is a [man page](doc/stolick.1) with more in-depth details, see `man stolick`.

If you want to know how you can integrate Stolick into your window manager/compositor,
see [doc/integration.md](doc/integration.md).

## Copying

Stolick is distributed under the terms of the [ISC license](doc/license.txt).
